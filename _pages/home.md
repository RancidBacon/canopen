---
title: ""
permalink: /
layout: splash
header:
  overlay_color: "#c30a14"
  actions:
    - label: "<i class='fab fa-gitlab'></i> GitLab"
      url: "https://gitlab.com/lely_industries/lely-core"
    - label: "<i class='fab fa-ubuntu'></i> Ubuntu PPA"
      url: "https://launchpad.net/~lely/+archive/ubuntu/ppa"
excerpt: >
  A feature-rich, open-source stack of industrial quality.<br/>
  <small>[Latest release: v2.2.2](release/v2.2.2/)</small>
feature_row:
  - image_path: /assets/images/gem-regular.svg
    title: "Feature-rich"
    excerpt: "Everything you need for industrial applications, such as automatic configuration and firmware updates. All compliant with the [CiA](https://www.can-cia.org/) standards."
    url: "/docs/standards/"
    btn_class: "btn--primary-outline"
    btn_label: "Learn more"
  - image_path: /assets/images/award-solid.svg
    title: "High code-quality"
    excerpt: "We compile without warnings, run a test suite on every build in the CI/CD pipeline and use half a dozen static code analyzers."
  - image_path: /assets/images/tools-solid.svg
    title: "Configurable"
    excerpt: "Everything is optional, both at compile time and runtime, making it no problem to run the stack on a microcontroller with only 32 kB RAM."
    url: "/docs/configuration/"
    btn_class: "btn--primary-outline"
    btn_label: "Learn more"
  - image_path: /assets/images/book-open-solid.svg
    title: "Well documented"
    excerpt: "Every function in the API is fully documented."
    url: "https://lely_industries.gitlab.io/lely-core/doxygen/"
    btn_class: "btn--primary-outline"
    btn_label: "Learn more"
  - image_path: /assets/images/tractor-solid.svg
    title: "Used in the field"
    excerpt: "... every day, by thousands of robots, on farms in dozens of countries."
  - image_path: /assets/images/creative-commons-nc-brands.svg
    title: "100% free"
    excerpt: "Open source and free to use, for any purpose. Licensed under the Apache License, Version 2.0."
    url: "/docs/license/"
    btn_class: "btn--primary-outline"
    btn_label: "Learn more"
---

## Why Lely CANopen?
{: .text-center}

According to an
[assessment](https://indico.esa.int/event/276/contributions/4539/attachments/3500/4630/W43_-_Karl_-_cis19.pdf)
by the European Space Agency,<br/>
Lely CANopen is<br/>
*extensive*, *feature-rich*, of *high code-quality*, *configurable* and
*well documented*.
{: .text-center}

{% include feature_row %}
