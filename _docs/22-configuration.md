---
title: "Build configuration"
permalink: /docs/configuration/
toc: true
---

Many features in the Lely core libraries can be disabled at compile time by
specifying flags to the `configure` script of the autotools build system or
defining the corresponding preprocessor macros.

## Language support

### C

Most C functions use `assert()` to validate their input. If you wish to disable
assertions, define the `NDEBUG` preprocessor macro.

Since C11, `struct timespec` is defined in `<time.h>`. When using a C99
compiler, the C11 and POSIX compatibility library (liblely-libc) defined this
struct in `<lely/libc/time.h>`. However, some C standard libraries (such as
[Newlib](https://sourceware.org/newlib/)) define this struct without properly
advertising it, leading to a compilation error. Define `__timespec_defined` to
suppress the definition in `<lely/libc/time.h>`.

Some functions in the Lely core libraries use variable-length arrays (VLAs) on
platforms that support it. To prevent stack overflows, VLAs are only used if the
size (in bytes) of the resulting array does not exceed a maximum value, given by
the `LELY_VLA_SIZE_MAX` preprocessor macro (default: 256).

### C++

Although the Lely core libraries are written in C, C++ interfaces are provided
for a subset of the functionality. These interfaces can be disabled with the
`--disable-cxx` option to `configure` or by defining the `LELY_NO_CXX`
preprocessor macro. This also disables the entire C++ CANopen application
library.

### Python

The Python bindings can be disabled by specifying the `--disable-python` option
to `configure`.

## Platform support

Multithreading support can be disabled by providing the `--disable-threads`
option to `configure` or by defining the `LELY_NO_THREADS` preprocessor macro.

On POSIX and Windows platforms, the utilities library (liblely-util) provides
functions to run a process in the background as a daemon/service.
This functionality can be disabled with the `--disable-daemon` option to
`configure` or by defining the `LELY_NO_DAEMON` preprocessor macro.

The I/O library (liblely-io2) provides a generic CAN message type suitable for
both CAN and CAN FD frames. If CAN FD support is not required, the message size
can be reduced by specifying the `--disable-canfd` option to `configure` or by
defining the `LELY_NO_CANFD` preprocessor macro.

**Note:** If `LELY_NO_CANFD` is defined when compiling the libraries, it
**must** also be defined when compiling applications using the `can_msg` struct.
{: .notice--warning}

### Windows

If the `WINVER`, `_WIN32_WINNT` or `NTDDI_VERSION` preprocessor macros are not
defined, `<lely/features.h>` (which is included by every header in the Lely core
libraries) defines them to `0x0601` (Windows 7, the minimum supported version).

`<lely/features.h>` also defines `WIN32_LEAN_AND_MEAN`, if not yet defined,
before including `<windef.h>`.
{: .notice--info}

### POSIX

[POSIX compatibility](https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap02.html#tag_02_01_03),
and the availability of optional features, is checked by inspecting the values
of the following preprocessor macros:

* `_POSIX_C_SOURCE`:
  used to check if the target platform is POSIX compatible.
* `_POSIX_MAPPED_FILES`:
  used to check for the availability of `mmap()` (from `<sys/mman.h>`).
* `_POSIX_PRIORITY_SCHEDULING`:
  used to check for the availability of `sched_yield()` (from `<sched.h>`) in
  the implementation of `thrd_yield()`.
* `_POSIX_THREADS`:
  used to check for the availability of POSIX threads (and `<pthread.h>`).
* `_POSIX_TIMERS`:
  used to check for the availability of `clock_getres()`, `clock_gettime()`,
  `clock_nanosleep()`, `clock_settime()` and `nanosleep()` in `<time.h>`.

## Event library

The run functions of polling event loop (defined in `<lely/ev/loop.h>`) require
an internal context (which is used, among other things, to wake up an event loop
from another thread). To minimize the overhead of allocating these contexts,
they are cached and reused, up to a maximum of `LELY_EV_LOOP_CTX_MAX_UNUSED`
(default: 16) unused contexts per event loop.

Support for multiple contexts is necessary because the polling event loop
supports nested run functions, i.e., calling a run function from a task that is
being executed by another run function.
{: .notice--info}

The [fiber](https://en.wikipedia.org/wiki/Fiber_%28computer_science%29) executor
runs every task in a coroutine. To minimize the overhead of creating coroutines
(and allocating their stack), terminated coroutines are cached and reused, up to
a maximum of `LELY_EV_FIBER_MAX_UNUSED` (default: 16) unused coroutines per
thread.

## CANopen library

If the `NDEBUG` preprocessor macro is *not* defined, the CANopen library defines
an internal `trace()` macro that is used to print debug information.

### Features

Much of the functionality of CANopen is optional and can be disabled to save
space on embedded devices. The CANopen library (liblely-co) supports the
following `configure` options (or preprocessor macros) to disable certain
features:

* `--disable-dcf` (`LELY_NO_CO_DCF`):
  disable EDS/DCF support (also disables the [CANopen control](/docs/coctl/)
  tool, [CANopen cat](/docs/cocat/) server and [DCF-to-C](/docs/dcf2c/) tool).
* `--disable-dcf-restore` (`LELY_NO_CO_DCF_RESTORE`):
  do not (re)store concise DCF of the application parameters.
* `--disable-obj-default` (`LELY_NO_CO_OBJ_DEFAULT`):
  disable default values in the object dictionary.
* `--disable-obj-file` (`LELY_NO_CO_OBJ_FILE`):
  disable UploadFile/DownloadFile support for the object dictionary.
* `--disable-obj-limits` (`LELY_NO_CO_OBJ_LIMITS`):
  disable minimum/maximum values in the object dictionary.
* `--disable-obj-name` (`LELY_NO_CO_OBJ_NAME`):
  disable names in the object dictionary.
* `--disable-obj-upload` (`LELY_NO_CO_OBJ_UPLOAD`):
  disable custom upload indication functions in the object dictionary.
* `--disable-sdev` (`LELY_NO_CO_SDEV`):
  disable static device description support (also disables the
  [DCF-to-C](/docs/dcf2c/) tool).
* `--disable-csdo` (`LELY_NO_CO_CSDO`):
  disable Client-SDO support (also disables master support).
* `--disable-rpdo` (`LELY_NO_CO_RPDO`):
  disable Receive-PDO support.
* `--disable-tpdo` (`LELY_NO_CO_TPDO`):
  disable Transmit-PDO support (also disables the
  [CANopen cat]({{ '/docs/cocat/' | relative_url }}) server).
* `--disable-sync` (`LELY_NO_CO_SYNC`):
  disable synchronization (SYNC) object support.
* `--disable-time` (`LELY_NO_CO_TIME`):
  disable time stamp (TIME) object support.
* `--disable-emcy` (`LELY_NO_CO_EMCY`):
  disable emergency (EMCY) object support.
* `--disable-lss` (`LELY_NO_CO_LSS`):
  disable Layer Setting Services (LSS) and protocols support.
* `--disable-wtm` (`LELY_NO_CO_WTM`):
  disable Wireless Transmission Media (WTM) support (also disables the
  [CAN-to-UDP](/docs/can2udp/) tool).
* `--disable-master` (`LELY_NO_CO_MASTER`):
  disable master support.
* `--disable-gw` (`LELY_NO_CO_GW`):
  disable gateway support (also disables ASCII gateway support).
* `--disable-gw-txt` (`LELY_NO_CO_GW_TXT`):
  disable ASCII gateway support (also disables the
  [CANopen control]({{ '/docs/coctl/' | relative_url }}) tool).

### Timeouts

The following preprocessor macros can be defined to change the default timeouts
used by an NMT master when booting a slave:

* `LELY_CO_NMT_TIMEOUT`: the default timeout (in milliseconds) for SDO requests
  issued by an NMT master (default: 100). The actual timeout can be changed at
  runtime with `co_nmt_set_timeout()`.
* `LELY_CO_NMT_BOOT_WAIT_TIMEOUT`: the timeout (in milliseconds) before an NMT
  master tries to boot a slave on error status B (default: 1000, see Fig. 4 in
  CiA 302-2 version 4.1.0).
* `LELY_CO_NMT_BOOT_SDO_RETRY`: the number of times an NMT master retries an SDO
  request on timeout (default: 3). This is used for SDO requests that may occur
  right after a slave receives a reset node or reset communication command,
  which might cause it to miss the request.
* `LELY_CO_NMT_BOOT_RTR_TIMEOUT`: the timeout (in milliseconds) after an NMT
  master sends a node guarding RTR to check the NMT state of a slave during
  booting (default: 100, see Fig. 9 in CiA 302-2 version 4.1.0).
* `LELY_CO_NMT_BOOT_RESET_TIMEOUT`: the timeout (in milliseconds) when waiting
  for a boot-up message after sending an NMT 'reset communication' command
  (default: 1000).
* `LELY_CO_NMT_BOOT_CHECK_TIMEOUT`: the time (in milliseconds) between
  successive checks of the flash status indication (1F57:01) or program control
  (1F51:01) sub-object of a slave during booting (default: 100, see Fig. 3 in
  CiA 302-3 version 4.1.0).

The following macros can be defined to change the default timeouts used by an
LSS master:

* `LELY_CO_LSS_INHIBIT`: The default inhbiit time (in multiples of 100
  microseconds) between successive CAN frames sent by an LSS master service
  (default: 10, which corresponds to 1 ms). The actual timeout can be changed at
  runtime with `co_lss_set_inhibit()`.
* `LELY_CO_LSS_TIMEOUT`: the default timeout (in milliseconds) when waiting for
  slaves to respond to an LSS request (default: 100). The actual timeout can be
  changed at runtime with `co_lss_set_timeout()`.

## C++ CANopen application library

Master and/or slave support for the C++ CANopen application library
(liblely-coapp) can be disabled with the following `configure` options (or
preprocessor macros):

* `--disable-coapp-master` (`LELY_NO_COAPP_MASTER`):
  disable C++ CANopen application master support.
* `--disable-coapp-slave` (`LELY_NO_COAPP_SLAVE`):
  disable C++ CANopen application slave support.
